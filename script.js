$.fn.preload = function() {
	this.each(function(){
        $('<img/>')[0].src = this;
    });
}
function isTouchDevice() {  
  try {  
    document.createEvent("TouchEvent");  
    return true;
  } catch (e) {  
    return false;  
  }  
}

function toggleFullScreen() {
    var doc = window.document;
    var docEl = doc.documentElement;

    var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
    var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
    } else {
        cancelFullScreen.call(doc);
    }
}

// var tierlist = [ [ {naut: name}, {naut: name} ] ]

$(document).ready(function() {
	var tierlistEl = $('#tierlist');
	// populate tierlist & generate tierlistByNauts
	var tierlistByNauts = {};
	for (let i = 0; i < tiers.length; i++) {
		let tier = tierlist[i];
		let row = $('<tr class="tier"><td class="tier-label"></td><td class="tier-nauts"></td></tr>');
		row.attr('id', 'tier-' + i);
		row.find('.tier-label').html(tiers[i].label);
		
		if (tier) {
			for (let j = 0; j < tier.length; j++) {
				let naut = tier[j];
				if (!nautsById[naut.naut]) {
					alert('Unknown naut id ' + naut.naut + ', skipping it.');
					continue;
				}
				
				tierlistByNauts[naut.naut] = naut;
				
				let el = $('<div class="naut card"></div>');
				el.css('background-image', 'url(nauts/' + naut.naut + '.png)');
				el.attr('title', nautsById[naut.naut].name);
				el.attr('data-naut', naut.naut);
				
				row.find('.tier-nauts').append(el);
			}
		}
		
		tierlistEl.append(row);
	}
	
	// naut information
	var nautModal = $('#naut-modal');
	nautModal.modal({
		complete: function () { window.location.hash = ''; }
	});
	var updateNautModal = function(naut) {
		var generalInfo = nautsById[naut.naut];
		nautModal.find('.naut-name').html(generalInfo ? generalInfo.name : naut.naut);
		nautModal.find('.naut-expansion').attr('src', generalInfo ? 'expansion/' + generalInfo.expansion + '.png' : '');
		nautModal.find('.naut-expansion').attr('title', generalInfo ? generalInfo.expansionName() : '');
		nautModal.find('.naut-wiki').attr('href', generalInfo ? generalInfo.wiki : '');
		
		var pros = nautModal.find('.naut-pros');
		pros.empty();
		for (var i = 0; i < naut.pros.length; i++) {
			var pro = naut.pros[i];
			var el = $('<li class="naut-pro"></li>');
			el.html(pro);
			pros.append(el);
		}
		
		var cons = nautModal.find('.naut-cons');
		cons.empty();
		for (var i = 0; i < naut.cons.length; i++) {
			var con = naut.cons[i];
			var el = $('<li class="naut-con"></li>');
			el.html(con);
			cons.append(el);
		}
	}
	$('.naut').click(function() {
		updateNautModal(tierlistByNauts[$(this).data('naut')]);
		// see https://stackoverflow.com/questions/12260279/scrolltop-not-working-in-android-mobiles
		nautModal.find('.modal-content').addClass("android-fix").scrollTop(0).removeClass("android-fix");
		nautModal.modal('open');
		window.location.hash = $(this).data('naut');
	});
	
	
	$('.tier-nauts').sortable({
		connectWith: '.tier-nauts',
		placeholder: 'naut-placeholder card indigo lighten-5'
	}).disableSelection();
	$('.tier-nauts').sortable('disable');
	
	var edit = false;
	if (!isTouchDevice()) {
		$('.tier-nauts').sortable('enable');
		edit = true;
		$('#mode').remove();
	}
	$('#mode').click(function () {
		if (!edit) {
			$('.tier-nauts').sortable('enable');
			
			$(this).find('i').html('search');
			$(this).addClass('green');
			$(this).removeClass('red');
			Materialize.toast('Edit mode enabled', 4000);
			edit = true;
		} else {
			$('.tier-nauts').sortable('disable');
			
			$(this).find('i').html('mode_edit');
			$(this).addClass('red');
			$(this).removeClass('green');
			Materialize.toast('View mode enabled', 4000);
			edit = false;
		}
	});
});