var tiers = [
	{ label: 'SS' },
	{ label: 'S' },
	{ label: 'A+' },
	{ label: 'A' },
	{ label: 'B+' },
	{ label: 'B' },
	{ label: 'C' },
	{ label: 'D' },
	{ label: '?' }
];

var Naut = function(id, expansion, name, wiki) {
	this.expansion = expansion;
	this.id = id;
	this.name = name;
	this.wiki = wiki;
}
Naut.prototype.expansionName = function() {
	switch (this.expansion) {
		case 'base':
			return 'Awesomenauts';
		case 'starstorm':
			return 'Awesomenauts: Starstorm';
		case 'overdrive':
			return 'Awesomenauts: Overdrive';
		default:
			return this.expansion;
	}
}

var nautsById = {
	"froggy-g": new Naut("froggy-g", "base", "Froggy G", "http://awesomenauts.gamepedia.com/Froggy_G"),
	"sheriff-lonestar": new Naut("sheriff-lonestar", "base", "Sheriff Lonestar", "http://awesomenauts.gamepedia.com/Sheriff_Lonestar"),
	"leon-chameleon": new Naut("leon-chameleon", "base", "Leon Chameleon", "http://awesomenauts.gamepedia.com/Leon_Chameleon"),
	"scoop": new Naut("scoop", "base", "Scoop of Justice", "http://awesomenauts.gamepedia.com/Scoop_of_Justice"),
	"gnaw": new Naut("gnaw", "base", "Gnaw", "http://awesomenauts.gamepedia.com/Gnaw"),
	"raelynn": new Naut("raelynn", "base", "Raelynn", "http://awesomenauts.gamepedia.com/Raelynn"),
	"ayla": new Naut("ayla", "base", "Ayla", "http://awesomenauts.gamepedia.com/Ayla"),
	"clunk": new Naut("clunk", "base", "Clunk", "http://awesomenauts.gamepedia.com/Clunk"),
	"voltar": new Naut("voltar", "base", "Voltar the Omniscient", "http://awesomenauts.gamepedia.com/Voltar_the_Omniscient"),
	"coco-nebulon": new Naut("coco", "base", "Coco Nebulon", "http://awesomenauts.gamepedia.com/Coco_Nebulon"),
	"skolldir": new Naut("skolldir", "base", "Sk�lldir", "http://awesomenauts.gamepedia.com/Sk%C3%B8lldir"),
	"yuri": new Naut("yuri", "base", "Yuri", "http://awesomenauts.gamepedia.com/Yuri"),
	"derpl-zork": new Naut("derpl-zork", "base", "Derpl Zork", "http://awesomenauts.gamepedia.com/Derpl_Zork"),
	"vinnie-&-spike": new Naut("vinnie-&-spike", "base", "Vinnie & Spike", "http://awesomenauts.gamepedia.com/Vinnie_%26_Spike"),
	"genji": new Naut("genji", "base", "Genji the Pollen Prophet", "http://awesomenauts.gamepedia.com/Genji_the_Pollen_Prophet"),
	"admiral-swiggins": new Naut("admiral-swiggins", "base", "Admiral Swiggins", "http://awesomenauts.gamepedia.com/Admiral_Swiggins"),
	"rocco": new Naut("rocco", "base", "Rocco", "http://awesomenauts.gamepedia.com/Rocco"),
	"ksenia": new Naut("ksenia", "base", "Ksenia", "http://awesomenauts.gamepedia.com/Ksenia"),
	"ix": new Naut("ix", "base", "Ix the Interloper", "http://awesomenauts.gamepedia.com/Ix_the_Interloper"),
	"deadlift": new Naut("deadlift", "base", "Deadlift", "http://awesomenauts.gamepedia.com/Deadlift"),
	"snork-gunk": new Naut("snork-gunk", "base", "Snork Gunk", "https://awesomenauts.gamepedia.com/Snork_Gunk"),
	"smiles": new Naut("smiles", "base", "Smiles", "https://awesomenauts.gamepedia.com/Smiles"),
	"commander-rocket": new Naut("commander-rocket", "base", "Commander Rocket", "https://awesomenauts.gamepedia.com/Commander_Rocket"),
	"qitara": new Naut("qitata", "base", "Qi'Tara", "https://awesomenauts.gamepedia.com/Qi'Tara"),
	"dizzy": new Naut("dizzy", "base", "Dizzy", "https://awesomenauts.gamepedia.com/Dizzy"),
	
	"ted-mcpain": new Naut("ted-mcpain", "starstorm", "Ted McPain", "http://awesomenauts.gamepedia.com/Ted_McPain"),
	"penny-fox": new Naut("penny-fox", "starstorm", "Penny Fox", "http://awesomenauts.gamepedia.com/Penny_Fox"),
	"sentry-x-58": new Naut("sentry", "starstorm", "Sentry X-58", "http://awesomenauts.gamepedia.com/Sentry_X-58"),
	"skree": new Naut("skree", "starstorm", "Skree", "http://awesomenauts.gamepedia.com/Skree"),
	"nibbs": new Naut("nibbs", "starstorm", "Nibbs", "http://awesomenauts.gamepedia.com/"),
	
	"professor-yoolip": new Naut("yoolip", "overdrive", "Professor Milton Yoolip", "http://awesomenauts.gamepedia.com/Professor_Milton_Yoolip"),
	"chucho-krokk": new Naut("chucho-krokk", "overdrive", "Chucho Krokk", "http://awesomenauts.gamepedia.com/Chucho_Krokk"),
	"jimmy-and-the-lux5000": new Naut("jimmy-and-the-lux5000", "overdrive", "Jimmy and the Lux5000", "http://awesomenauts.gamepedia.com/Jimmy_And_The_Lux5000"),
	"max-focus": new Naut("max-focus", "overdrive", "Max Focus", "http://awesomenauts.gamepedia.com/Max_Focus")
};

// old content, from july 2017
// var tierlist = [[],[{"naut":"froggy","pros":["waveclear","gap-closing","burst","immunity","high dmg","short aoe-stun","mobility","strong early","good scaling","backdoorpotential","push"],"cons":["aa-bug","weak aa"]},{"naut":"scoop","pros":["2xheal","lifesteal","high health","high aoe-dmg","pressure","ranged aoe-snare","good scaling","offensive siege","poke"],"cons":["unmobil"]},{"naut":"raelynn","pros":["waveclear","high long ranged aoe burst dmg","easy snowball","very strong aoe slow","mobility","push"],"cons":["dmg stopable","unmobil during dmg"]},{"naut":"coco","pros":["safe waveclear","high dmg","aoe slow","mobility","good scaling","offensive siege","poke","ranged aoe burst"],"cons":[""]},{"naut":"yoolip","pros":["safe waveclear","high dmg","good scaling","defensive siege","poke","ranged long stun","strong early","minion heal","push","low cd"],"cons":["weak against cc","weak when using cc"]}],[{"naut":"genji","pros":["strong waveclear","hard cc","high aoe-aa-dmg","dot-dmg","team buffs","strong push"],"cons":["unmobil"]}],[{"naut":"leon","pros":["high aa-dmg","lifesteal","safe gap-closing","invisibility","pressure","backdoor","good scaling","snowball","push"],"cons":["weak against pressure","weak against poke"]},{"naut":"gnaw","pros":["aoe slow","aoe-dmg","versatile","dot- dmg","lifesteal","huge area control","snowball","strong early"],"cons":["weak from behind","dmg not confirmable (espiacially in late)"]},{"naut":"ayla","pros":["high aa-dmg","lifesteal","burst","pressure","backdoorpotential","aoe-dmg","slow","push","utility"],"cons":["weak against pressure","weak against cc"]},{"naut":"vinnie-and-spike","pros":["safe waveclear","high dmg","aoe slow","mobility","very good scaling","offensive siege","poke","backdoorpotential","aoe burst"],"cons":["weak against cc","weak aginst poke"]},{"naut":"swiggins","pros":["high pressure","high aa-dmg","high aoe-dmg","stun","hard cc","gap closing","immunity","push","snowball","good scaling","aoe burst"],"cons":["weak against poke","weak against cc"]},{"naut":"rocco","pros":["pressure","ranged aoe slow","high aa-dmg","poke"],"cons":["unmobil","weak against poke","weak against cc"]},{"naut":"sentry","pros":["ranged aoe cc","pressure","waveclear","teleport","strong offensive siege","high health"],"cons":["team dependant","unmobil"]},{"naut":"skree","pros":["safe waveclear","safe+ strong push","heal","strong defensive siege","push","burst","pressure"],"cons":["weak against cc"]},{"naut":"nibbs","pros":["extremely high aoe dmg","gap-closing","strong early","extremly strong offensive siege","pressure","backdoor","snowball"],"cons":["dmg notconfirmable","weak against poke","unmobil during dmg"]},{"naut":"max","pros":["safe waveclear","long ranged aoe slow","strong defensive siege","push"],"cons":["weak against poke","weak againstn cc"]}],[{"naut":"lonestar","pros":["safe waveclear","safe+ strong push","heal","strong defensive siege","burst"],"cons":["very unmobil"]},{"naut":"ted","pros":["high aa-dmg","safe waveclear","push","aoe dmg","low cd"],"cons":["dmg not confirmable","weak against poke"]},{"naut":"penny","pros":["mobility","short aoe-stun","gap-closing","burst","very good scaling","waveclear","high aoe-dmg","push","backdoorpotential","snowball","low cd"],"cons":["weak against pressure","weak against cc","weak against poke","weak early"]}],[{"naut":"clunk","pros":["high pressure","high aa-dmg","heal","high health","high aoe-dmg","snare","offensive siege","push","burst"],"cons":["very unmobil","self dmg","weak against cc"]},{"naut":"voltar","pros":["high aoe heal","insane snowball","high dmg","minion heal"],"cons":["unmobil","team dependant","low health"]},{"naut":"skolldir","pros":["high aa-dmg","aa-stun","aoe dot","aoe snare","sustain","aoe displace","push"],"cons":["unmobil","dmg not confirmable "]},{"naut":"derpl","pros":["waveclear","high health","strong single area control","strong pressure","ranged burst","strong offensive and defensive siege","snare","high dmg","push"],"cons":["extremely unmobil","very weak against poke"]},{"naut":"ix","pros":["team heal","immunity","team immunity","displacemet","lifesteal","aoe- aa","push"],"cons":["weak against pressure"]},{"naut":"jimmy","pros":["aoe-aa-dmg","push","poke","displacement","mobility","high health"],"cons":["weak against poke","dmg not confirmable"]}],[{"naut":"yuri","pros":["flying","waveclear","burst","aoe slow","aoe heal","strong single area control"],"cons":["unmobil","dmg not confirmable","weak against poke"]},{"naut":"ksenia","pros":["gap-closing","extremely high damage","silence","extremely good scaling","immunity","push","backdoorpotntial","high aa-dmg","invisibility","push"],"cons":["weak early","weak against poke","weak against cc","weak against pressure"]}],[{"naut":"deadlift","pros":["sustain","high aa-dmg","extreme strong aoe shield","gap closing","strong against burst","push","high health"],"cons":["weak against poke","weak against pressure","extremely team dependant"]}],[{"naut":"chucho","pros":["???"],"cons":["???"]}]];
var tierlist = [[{"naut":"raelynn","pros":["safe waveclear","high long ranged aoe burst dmg","easy snowball","very strong aoe slow","mobility","push"],"cons":["dmg stopable","immobile during dmg"]}],[{"naut":"froggy-g","pros":["waveclear","gap-closing","burst","immunity","high dmg","short aoe-stun","mobility","strong early","good scaling","backdoor","push"],"cons":[""]},{"naut":"scoop","pros":["3xheal","lifesteal","high health","high aoe-dmg","pressure","ranged aoe-snare","good scaling","offensive siege","poke","team aoe heal"],"cons":["immobile"]},{"naut":"coco-nebulon","pros":["save waveclear","high dmg","aoe slow","mobility","good scaling","offensive siege","poke","ranged aoe burst","versatile"],"cons":[""]},{"naut":"professor-yoolip","pros":["save waveclear","high dmg","good scaling","defensive siege","poke","ranged long stun","strong early","minion heal","push","low cd"],"cons":["weak against cc","weak when using cc","team dependant"]}],[{"naut":"sheriff-lonestar","pros":["safe waveclear","safe+ strong push","heal","strong defensive siege","burst"],"cons":["very immobile"]},{"naut":"gnaw","pros":["aoe slow","aoe-dmg","versatile","dot- dmg","lifesteal","huge area control","snowball","strong early"],"cons":["weak from behind","dmg not confirmable"]},{"naut":"genji","pros":["strong waveclear","hard cc","high aoe-aa-dmg","dot-dmg","team buffs","strong push"],"cons":["immobile","team dependant"]},{"naut":"nibbs","pros":["extremely high aoe dmg","gap-closing","strong early","extremly strong offensive siege","pressure","backdoor","snowball","strong scaling","good sustain"],"cons":["dmg notconfirmable","weak against poke","immobile during dmg","weak early"]},{"naut":"snork-gunk","pros":["fast long range gap closing","high aoe burst","mobility"],"cons":["weak from behind","weak against pressure"]}],[{"naut":"leon-chameleon","pros":["high aa-dmg","lifesteal","safe gap-closing","invisibility","pressure","backdoor","good scaling","snowball","push"],"cons":["weak against pressure","weak against poke"]},{"naut":"ayla","pros":["high aa-dmg","lifesteal","burst","pressure","backdoorpotential","aoe-dmg","slow","push","utility"],"cons":["weak against pressure","weak against cc"]},{"naut":"vinnie-&-spike","pros":["save waveclear","high dmg","aoe slow","mobility","very good scaling","offensive siege","poke","backdoor","aoe burst"],"cons":["weak against cc","weak aginst poke"]},{"naut":"rocco","pros":["pressure","ranged aoe slow","high aa-dmg","good poke"],"cons":["weak against poke","weak against cc"]},{"naut":"skree","pros":["safe waveclear","safe+ strong push","heal","strong defensive siege","push","burst","pressure"],"cons":["weak against cc"]},{"naut":"max-focus","pros":["save waveclear","long ranged aoe slow","strong defensive siege","push","flying"],"cons":["weak against poke","weak against cc"]}],[{"naut":"admiral-swiggins","pros":["high pressure","high aa-dmg","high aoe-dmg","stun","hard cc","gap closing","immunity","push","snowball","good scaling","aoe burst"],"cons":["weak against poke","weak against cc","weak from behind"]},{"naut":"ted-mcpain","pros":["high aa-dmg","safe waveclear","push","aoe dmg","low cd"],"cons":["dmg not confirmable","weak against poke"]},{"naut":"penny-fox","pros":["mobility","short aoe-stun","gap-closing","burst","very good scaling","waveclear","high aoe-dmg","push","backdoorpotential","snowball","low cd"],"cons":["weak against pressure","weak against cc","weak against poke","weak early"]},{"naut":"smiles","pros":["dot-dmg","strong cc","aoe dmg","pressure"],"cons":["weak from behind","team dependant"]}],[{"naut":"clunk","pros":["high pressure","high aa-dmg","heal","high health","high aoe-dmg","snare","offensive siege","push","burst"],"cons":["very immobile","self dmg","weak against cc"]},{"naut":"voltar","pros":["high aoe heal","good snowball","high dmg","minion heal"],"cons":["immobile","very team dependant","low health"]},{"naut":"skolldir","pros":["high aa-dmg","aa-stun","aoe dot","aoe snare","sustain","aoe displace","push"],"cons":["immobile","dmg not confirmable","map dependant "]},{"naut":"derpl-zork","pros":["waveclear","high health","strong single area control","strong pressure","ranged burst","strong offensive and defensive siege","snare","high dmg","push"],"cons":["extremely immobile","very weak against poke"]},{"naut":"ix","pros":["team heal","immunity","team immunity","displacemet","lifesteal","aoe- aa","push"],"cons":["weak against pressure"]},{"naut":"sentry-x-58","pros":["ranged aoe cc","pressure","waveclear","teleport","strong offensive siege","high health"],"cons":["team dependant","immobile"]},{"naut":"jimmy-and-the-lux5000","pros":["aoe-aa-dmg","push","poke","displacement","mobility","high health"],"cons":["weak against poke","dmg not confirmable"]},{"naut":"commander-rocket","pros":["safe push","strong poke","good zone denial and control"],"cons":["extremely weak against poke and cc","no sustain"]},{"naut":"qitara","pros":["gap-closing","very high aa-damage","silence","good scaling","immunity","push","teleport","push","dot-dmg"],"cons":["weak against poke","weak against cc"]}],[{"naut":"yuri","pros":["flying","waveclear","burst","aoe slow","aoe heal","strong single area control"],"cons":["immobile","dmg not confirmable","weak against poke"]},{"naut":"ksenia","pros":["gap-closing","extremely high damage","silence","extremely good scaling","immunity","push","backdoorpotntial","high aa-dmg","invisibility","push"],"cons":["weak early","weak against poke","weak against cc","weak against pressure"]},{"naut":"chucho-krokk","pros":["area control","mobility","aoe poke","push"],"cons":["extremely weak from behind","immobile if turret is destroyed","animation heavy"]},{"naut":"deadlift","pros":["sustain","high aa-dmg","extreme strong aoe shield","gap closing","strong against burst","push","high health"],"cons":["weak against pressure","extremely team dependant"]}],[{"naut":"dizzy","pros":["extremely high mobility","poke"],"cons":["low dmg","dmg not confirmable","weak against poke and cc","weak from behind"]}],[]];

/*
// still old content, july 2017
var tierlist = [  
  [  

  ],
  [  
    {  
      "naut":"froggy",
      "pros":[  
        "waveclear",
        "gap-closing",
        "burst",
        "immunity",
        "high dmg",
        "kurzer aoe-stun",
        "mobility",
        "starkes early",
        "gutes skaling",
        "backdoor",
        "push"
      ],
      "cons":[  
        "aa-bug",
        "schwache aa"
      ]
    },
    {  
      "naut":"scoop",
      "pros":[  
        "2xheal",
        "lifesteal",
        "high health",
        "high aoe-dmg",
        "pressure",
        "ranged aoe-snare",
        "gutes skaling",
        "offensiver siege",
        "poke"
      ],
      "cons":[  
        "unmobil"
      ]
    },
    {  
      "naut":"raelynn",
      "pros":[  
        "waveclear",
        "high long ranged burst",
        "easy snowball",
        "aoe slow",
        "mobility",
        "push"
      ],
      "cons":[  
        "dmg abbrechbar"
      ]
    },
    {  
      "naut":"coco",
      "pros":[  
        "save waveclear",
        "high dmg",
        "aoe slow",
        "mobility",
        "gutes skaling",
        "offensiver siege",
        "poke"
      ],
      "cons":[]
    },
    {  
      "naut":"yoolip",
      "pros":[  
        "save waveclear",
        "high dmg",
        "gutes skaling",
        "defensiver siege",
        "poke",
        "high cc",
        "starkes early",
        "minion heal",
        "push",
        "low cd"
      ],
      "cons":[  
        "schwach gegen cc",
        "unmobil im cc"
      ]
    }
  ],
  [  
    {  
      "naut":"genji",
      "pros":[  
        "strong waveclear",
        "hard cc",
        "high aoe-aa-dmg",
        "dot-dmg",
        "team buffs",
        "strong push"
      ],
      "cons":[  
        "unmobil"
      ]
    }
  ],
  [  
    {  
      "naut":"leon",
      "pros":[  
        "high aa-dmg",
        "lifesteal",
        "burst",
        "safe gap-closing",
        "invisibility",
        "pressure",
        "backdoor",
        "gutes skaling",
        "snowball",
        "push"
      ],
      "cons":[  
        "schwach gegen pressure",
        "schwach gegen poke"
      ]
    },
    {  
      "naut":"gnaw",
      "pros":[  
        "aoe slow",
        "aoe-dmg",
        "versatile",
        "dot- dmg",
        "lifesteal",
        "huge area control",
        "snowball",
        "strong early"
      ],
      "cons":[  
        "schlecht wenn behind",
        "dmg nicht confirmable"
      ]
    },
    {  
      "naut":"vinnie-and-spike",
      "pros":[  
        "defensiver siege",
        "sehr starker offensiver siege",
        "high aoe burst",
        "mobility",
        "fliegen",
        "invisibility",
        "gutes skaling",
        "waveclear",
        "low cd"
      ],
      "cons":[  
        "schwach gegen cc"
      ]
    },
    {  
      "naut":"swiggins",
      "pros":[  
        "high pressure",
        "high aa-dmg",
        "high aoe-dmg",
        "stun",
        "hard cc",
        "gap closing",
        "immunity",
        "push",
        "snowball"
      ],
      "cons":[  
        "unmobil",
        "schwach gegen poke"
      ]
    },
    {  
      "naut":"rocco",
      "pros":[  
        "pressure",
        "ranged aoe slow",
        "high aa-dmg",
        "poke"
      ],
      "cons":[  
        "unmobil",
        "schwach gegen cc"
      ]
    },
    {  
      "naut":"sentry",
      "pros":[  
        "ranged aoe cc",
        "pressure",
        "waveclear",
        "teleport",
        "starker offensiver siege",
        "high health"
      ],
      "cons":[  
        "teamabh�ngig",
        "unmobil"
      ]
    },
    {  
      "naut":"skree",
      "pros":[  
        "safe waveclear",
        "safe+ strong push",
        "heal",
        "starker defensiver siege",
        "push",
        "burst",
        "pressure"
      ],
      "cons":[  
        "schwach gegen cc"
      ]
    },
    {  
      "naut":"nibbs",
      "pros":[  
        "extremely high aoe dmg",
        "gap-closing",
        "starkes early",
        "extrem starker offensiver siege",
        "pressure",
        "backdoor",
        "snowball"
      ],
      "cons":[  
        "dmg nicht confirmable",
        "schwach gegen poke",
        "unmobil im dmg"
      ]
    },
    {  
      "naut":"max",
      "pros":[  
        "save waveclear",
        "ranged aoe slow",
        "extrem starker defensiver siege",
        "push"
      ],
      "cons":[  
        "schwach gegen poke",
        "schwach gegen cc"
      ]
    }
  ],
  [  
    {  
      "naut":"lonestar",
      "pros":[  
        "safe waveclear",
        "safe+ strong push",
        "heal",
        "starker defensiver siege"
      ],
      "cons":[  
        "sehr unmobil"
      ]
    },
    {  
      "naut":"ted",
      "pros":[  
        "high aa-dmg",
        "safe waveclear",
        "push",
        "aoe dmg",
        "low cd"
      ],
      "cons":[  
        "dmg nicht confirmable",
        "schwach gegen poke"
      ]
    }
  ],
  [  
    {  
      "naut":"ayla",
      "pros":[  
        "high aa-dmg",
        "lifesteal",
        "burst",
        "pressure",
        "backdoor",
        "aoe-dmg",
        "slow",
        "push"
      ],
      "cons":[  
        "schwach gegen cc",
        "schwach gegen pressure"
      ]
    },
    {  
      "naut":"clunk",
      "pros":[  
        "high pressure",
        "high aa-dmg",
        "heal",
        "high health",
        "high aoe-dmg",
        "snare",
        "offensiver siege",
        "push"
      ],
      "cons":[  
        "sehr unmobil",
        "self dmg",
        "schwach gegen cc"
      ]
    },
    {  
      "naut":"voltar",
      "pros":[  
        "high aoe heal",
        "insane snowball",
        "high dmg",
        "minion heal"
      ],
      "cons":[  
        "unmobil",
        "teamabh�ngig",
        "low health"
      ]
    },
    {  
      "naut":"skolldir",
      "pros":[  
        "high aa-dmg",
        "aa-stun",
        "aoe dot",
        "aoe snare",
        "sustain",
        "aoe displace",
        "push"
      ],
      "cons":[  
        "unmobil",
        "dmg nicht confirmable"
      ]
    },
    {  
      "naut":"derpl",
      "pros":[  
        "waveclear",
        "high health",
        "strong single area control",
        "strong pressure",
        "ranged burst",
        "starker offensiver+defensiver siege",
        "snare",
        "high dmg",
        "push"
      ],
      "cons":[  
        "extrem unmobil",
        "schwach gegen cc",
        "schwach gegen poke"
      ]
    },
    {  
      "naut":"ix",
      "pros":[  
        "team heal",
        "immunity",
        "team immunity",
        "displacemet",
        "lifesteal",
        "aoe- aa",
        "push"
      ],
      "cons":[  
        "schwach gegen pressure"
      ]
    },
    {  
      "naut":"penny",
      "pros":[  
        "mobility",
        "kurzer aoe-stun",
        "gap-closing",
        "burst",
        "gutes skaling",
        "waveclear",
        "high aoe-dmg",
        "push",
        "backdoor",
        "snowball",
        "low cd"
      ],
      "cons":[  
        "schwach gegen pressure",
        "schwach gegen cc",
        "schwach gegen poke",
        "schwaches early"
      ]
    },
    {  
      "naut":"jimmy",
      "pros":[  
        "aoe-aa-dmg",
        "push",
        "poke",
        "displace",
        "mobility"
      ],
      "cons":[  
        "schwach gegen poke",
        "schwach gegen cc"
      ]
    }
  ],
  [  
    {  
      "naut":"yuri",
      "pros":[  
        "fliegen",
        "waveclear",
        "burst",
        "aoe slow",
        "aoe heal",
        "strong single area control"
      ],
      "cons":[  
        "unmobil",
        "dmg nicht confirmable",
        "schwach gegen poke"
      ]
    },
    {  
      "naut":"ksenia",
      "pros":[  
        "gap-closing",
        "extremely high damage",
        "slow",
        "silence",
        "gutes skaling",
        "immunity",
        "push",
        "backdoor",
        "high aa-dmg",
        "invisibility",
        "push"
      ],
      "cons":[  
        "schwaches early",
        "schwach gegen poke",
        "schwach gegen cc",
        "schwach gegen pressure"
      ]
    }
  ],
  [
  ],
  [
    { "naut":"chucho", pros:[], cons:[] },
	{ "naut":"deadlift", pros:[], cons:[] }
  ]
];
*/
